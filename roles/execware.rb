name 'execware'
description "Testbed configuration for execware"
run_list  "recipe[apt]", "recipe[java]", "recipe[paasage]", "recipe[execware]"
override_attributes(
    'java' => {
        'install_flavor' => 'oracle',
        'jdk_version' => '8',
    },
    'mysql' => {
        'server_root_password' => 'paasage'
    }
)
