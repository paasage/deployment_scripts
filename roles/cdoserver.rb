name 'cdoserver'
description 'Testbed configuration for profiler'
run_list  'recipe[apt]', 'recipe[java]', 'recipe[paasage]',  'recipe[metabase]'
override_attributes(
    'java' => {
        'install_flavor' => 'oracle',
        'jdk_version' => '8',
    },
    'mysql' => {
        'server_root_password' => 'paasage'
    }
)
