name 'paasage'
description 'Testbed configuration for profiler'
run_list  "recipe[apt]", "recipe[paasage]","recipe[profiler]","recipe[metabase]", "recipe[reasoner]",  "recipe[adapter]", "recipe[webmasterscript]",  "recipe[execware]"
override_attributes(
    'java' => {
        'install_flavor' => 'oracle',
        'jdk_version' => '8',
    },
    'mysql' => {
        'server_root_password' => 'paasage'
    }
)
