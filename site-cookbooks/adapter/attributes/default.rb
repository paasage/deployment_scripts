default['adapter']['adaptation_manager']['application_name'] = 'adaptationmanager'
default['adapter']['adaptation_manager']['version'] = 'default'
default['adapter']['adaptation_manager']['vm_options']='-Xms512M -Xmx1024M -Xss1M -XX:MaxPermSize=512M'
