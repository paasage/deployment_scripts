#
# Cookbook Name:: webmasterscript
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#



install_user          = node['paasage']['installation_user']
application_name      = node['webmasterscript']['application_name']
clone_url             = node['webmasterscript']['clone_url']
git_scheme            = node['webmasterscript']['git_scheme']
git_username          = node['webmasterscript']['git_username']
git_password          = node['webmasterscript']['git_password']
clone_url_aws         = "#{node[:webmasterscript][:clone_url_aws]}"
branch_webmasterscript = "#{node[:webmasterscript][:branch]}"


def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end




if node.attribute?('installation_dir')
  installation_dir      = node['webmasterscript']['installation_dir']
else
  installation_dir      = "/home/#{install_user}/#{application_name}"
end


directory installation_dir do
  action :create
  mode "0755"
  owner install_user
  group install_user
  recursive true
end

apt_package 'git' do
  action :install
end

apt_package 'supervisor' do
  action :install
end
apt_package 'python-pip' do
  action :install
end


bash 'install-python-deps' do
  cwd installation_dir
  user 'root'
  group 'root'
  code <<-EOH
    pip install flask
    pip install flask_bootstrap
  EOH
end

# clone repositories
bash "clone-#{application_name}" do
  cwd installation_dir
  user install_user
  group install_user
  code <<-EOH
    rm -rf #{installation_dir}/#{application_name}
    mkdir -p #{installation_dir}/#{application_name}
    cd #{installation_dir}/#{application_name}
    git clone #{git_scheme}#{git_username}:#{git_password}@#{clone_url}
    git clone --branch #{branch_webmasterscript} #{clone_url_aws}
  EOH
end

#
template "/etc/supervisor/conf.d/#{application_name}.conf" do
  source 'flask.supervisord.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :install_user     => install_user,
                :application_name => application_name,
                :path             => installation_dir,
            })
end

service 'supervisor' do
  supports :stop => true, :start => true, :restart => true
  action [ :enable, :restart ]
end
