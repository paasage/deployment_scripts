

install_user          = "#{node[:paasage][:installation_user]}"
application_name      = "#{node[:paasage][:scalarm_integration_tests][:application_name]}"
dist_url              = "#{node[:paasage][:scalarm_integration_tests][:dist_url]}"
dist_name             = "#{node[:paasage][:scalarm_integration_tests][:dist_name]}"
vm_options            = "#{node[:paasage][:scalarm_integration_tests][:vm_options]}"


def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end




if node.attribute?('installation_dir')
  installation_dir      = "#{node[:paasage][:installation_dir]}"
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:paasage][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end

install_user_uid = uid_of_user install_user

if node.attribute?('pid_file_path')
  pid_file_path      = "#{node[:paasage][:pid_file_path]}"
else
  pid_file_path      = "/run/paasage/#{application_name}.pid"
end



directory "#{installation_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end

directory "#{config_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end


remote_file "#{installation_dir}/#{application_name}.jar" do
  source "#{dist_url}/#{dist_name}.jar"
  owner "#{install_user}"
  group "#{install_user}"
  mode "0644"
  action :create
end