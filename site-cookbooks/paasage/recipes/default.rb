#
# Cookbook Name:: paasage
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'apt::cacher-client'
include_recipe 'java'
include_recipe 'maven'
include_recipe 'etcd'

paasage_config_dir     = '/etc/paasage'
cdo_server_hostname    = node['paasage']['cdo_server']['hostname']
cdo_server_port        = node['paasage']['cdo_server']['port']
install_user           = node['paasage']['installation_user']
paasage_version        = node['paasage']['version']
paasage_version_suffix = node['paasage']['version_suffix']
paasage_version_branch = node['paasage']['version_branch']

if paasage_version_branch != ''
  paasage_version_branch = '-' + paasage_version_branch
end
if paasage_version_suffix != ''
  paasage_version_suffix = '-' + paasage_version_suffix
end
version = "#{paasage_version}#{paasage_version_branch}#{paasage_version_suffix}"

apt_package "jsvc" do
  action :install
end

bash "set-default-java8" do
  user 'root'

  code <<-EOF
  update-java-alternatives  -s java-8-oracle-amd64
  EOF
end



directory "#{paasage_config_dir}" do
  action :create
  mode '0755'
  owner 'root'
  group 'root'
  recursive true
end




template "/etc/profile.d/90-paasage.sh" do
  source 'profile.d-90-paasage.sh'
  owner 'root'
  group 'root'
  mode '0755'
  variables({
                :host =>  'localhost',
            })
end


template "#{paasage_config_dir}/eu.paasage.mddb.cdo.client.properties" do
  source 'eu.paasage.wp4.client.properties.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :hostname => cdo_server_hostname,
                :port     => cdo_server_port
            })
end

template "#{paasage_config_dir}/masterscript.sh" do
  source 'masterscript.sh.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables({
                :resource_name => node['paasage']['resource_name'],
                :install_user => install_user,
                :version => version
            })
end

template "#{paasage_config_dir}/mysqldump.sh" do
  source 'mysqldump.sh.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables({
                :resource_name => "#{node[:paasage][:resource_name]}",
                :install_user => "#{install_user}"
            })
end

template "#{paasage_config_dir}/download_useless_stuff.sh" do
  source 'download_useless_stuff.sh.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables({
                :resource_name => "#{node[:paasage][:resource_name]}"
            })
end



directory "/home/#{install_user}/.ssh" do
  action :create
  owner install_user
  group install_user
  mode '0700'
  recursive true
end


template "/home/#{install_user}/.ssh/git-deploy-key" do
  source 'git-deploy-key.erb'
  owner install_user
  group install_user
  mode '0600'
end

template "/home/#{install_user}/.ssh/config" do
  source 'ssh-config.erb'
  owner install_user
  group install_user
  mode '0600'
end

#rabbitmq_user "guest" do
#  action :delete
#end

#rabbitmq_user "#{node[:paasage][:rabbitmq][:paasage_user]}" do
#  password "#{node[:paasage][:rabbitmq][:paasage_password]}"
#  action :add
#end

#rabbitmq_vhost "#{node[:paasage][:rabbitmq][:paasage_vhost]}" do
#  action :add
#end

#rabbitmq_user "#{node[:paasage][:rabbitmq][:paasage_user]}" do
#  vhost "#{node[:paasage][:rabbitmq][:paasage_vhost]}"
#  permissions ".* .* .*"
#  action :set_permissions
#end

template "/etc/init/etcd.conf" do
  source 'etcd-alt.conf.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables({
                :ip_address => "#{node['ipaddress']}",
                :port => '4001'
            })
end
