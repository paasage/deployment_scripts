
default['paasage']['cdo_server']['hostname'] = 'localhost'
default['paasage']['cdo_server']['port'] = '2036'
default['paasage']['resource_name'] = 'ScalarmDemo'
default['paasage']['installation_user'] = 'vagrant'
default['paasage']['default_group_id'] = 'org.ow2.paasage'
default['paasage']['version'] = '2015.9.1'
default['paasage']['version_suffix'] =  'SNAPSHOT'
default['paasage']['version_branch'] =  'master'


default['java']['install_flavor'] ='oracle'
default['java']['jdk_version'] ='8'
default['java']['oracle']['accept_oracle_download_terms']  = true
default['java']['ark_timeout']= 3600

default['maven']['install_java'] = false
default['mysql']['server_root_password']= 'paasage'


default['paasage']['scalarm_integration_tests']['dist_url'] = 'http://jenkins.paasage.cetic.be/job/SCALARM_INTEGRATION_TESTS/lastSuccessfulBuild/artifact/target/'
default['paasage']['scalarm_integration_tests']['dist_name'] = 'scalarmintegrationtests-1.0-jar-with-dependencies'
default['paasage']['scalarm_integration_tests']['application_name'] = 'scalarmintegrationtests'
default['paasage']['scalarm_integration_tests']['vm_options']='-Xms512M -Xmx1024M -Xss1M'

default['paasage']['rabbitmq']['paasage_vhost'] = '/paasage'
default['paasage']['rabbitmq']['paasage_user'] = 'paasage'
default['paasage']['rabbitmq']['paasage_password'] = 'paasage2015'

default['maven']['repositories'] = [ 'http://repository.ow2.org/nexus/content/repositories/snapshots/','http://repository.ow2.org/nexus/content/repositories/releases/']
default['maven']['install_java'] = false

default['etcd']['install_method']='binary'
default['etcd']['version'] = '2.1.1'
default['etcd']['sha256'] = 'e02e4c699f53e27718778fb1db2b339252d855ec9807650525619f279c9110e9'
default['etcd']['snapshot'] = false

default['etcd']['addr'] = node[:ipaddress]
default['etcd']['name'] = node[:ipaddress]
default['etcd']['peer_addr'] =''
default['etcd']['args'] = ''
default['etcd']['discovery'] ='   '
