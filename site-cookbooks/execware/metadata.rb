name             "execware"
maintainer       "Example Com"
maintainer_email "ops@example.com"
license          "Apache 2.0"
description      "Installs/Configures execware"

version          "0.1"

depends          "mysql"
depends          "database"
depends          "deploy-play"
depends          "zip"
depends          "apache2"