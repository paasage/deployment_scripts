



install_user          = node['paasage']['installation_user']
group_id              = node['paasage']['default_group_id']

application_name      = "#{node[:execware][:backend][:application_name]}"
version               = "#{node[:execware][:backend][:version]}"

paasage_version        = node['paasage']['version']
paasage_version_suffix = node['paasage']['version_suffix']
paasage_version_branch = node['paasage']['version_branch']

if version == 'default'
  if paasage_version_branch != ''
    paasage_version_branch = '-' + paasage_version_branch
  end
  if paasage_version_suffix != ''
    paasage_version_suffix = '-' + paasage_version_suffix
  end
  version = "#{paasage_version}#{paasage_version_branch}#{paasage_version_suffix}"
end

def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end

if node.attribute?('installation_dir')
  installation_dir      = node['execware']['installation_dir']
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:execware][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end


if node.attribute?('pid_file_path')
  pid_file_path      = "#{node[:execware][:pid_file_path]}"
else
  pid_file_path      = "/run/paasage/#{application_name}.pid"
end

directory installation_dir do
  action :create
  mode '0755'
  owner install_user
  group install_user
  recursive true
end

directory config_dir do
  action :create
  mode '0755'
  owner install_user
  group install_user
  recursive true
end

maven application_name do
  group_id    group_id
  version     version
  classifier  'jar-with-dependencies'
  dest        installation_dir
  owner       install_user
  mode        '0644'
end


template '/etc/paasage/eu.paasage.execware.backend.properties' do
  source 'eu.paasage.execware.backend.properties.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :rabbitmq_vhost    => node[:paasage][:rabbitmq][:paasage_vhost],
                :rabbitmq_user     => node[:paasage][:rabbitmq][:paasage_user],
                :rabbitmq_password => node[:paasage][:rabbitmq][:paasage_password],
            })
end


template "/etc/init.d/#{application_name}" do
  source 'execware.backend.init.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :installation_user  => install_user,
                :application_name   => application_name,
                :installation_dir   => installation_dir,
                :jar_file_name      => "#{application_name}-#{version}-jar-with-dependencies.jar"
            })
end


service "#{application_name}" do
  supports :stop => true, :start => true, :restart => true
  action [ :enable, :restart ]
end

