



install_user          = node['paasage']['installation_user']
group_id              = 'de.uniulm.omi.cloudiator'

application_name      = "#{node[:execware][:execware_init][:application_name]}"
version               = "#{node[:execware][:execware_init][:version]}"
vm_options            = "#{node[:execware][:execware_init][:vm_options]}"
classifier            = 'jar-with-dependencies'

if node.attribute?('installation_dir')
  installation_dir      = node['execware']['installation_dir']
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:execware][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end


directory installation_dir do
  action :create
  mode '0755'
  owner install_user
  group install_user
  recursive true
end

directory config_dir do
  action :create
  mode '0755'
  owner install_user
  group install_user
  recursive true
end

maven application_name do
  group_id    group_id
  version     version
  dest        installation_dir
  owner       install_user
  classifier  "#{classifier}"
  mode        '0644'
  repositories [ 'https://omi-dev.e-technik.uni-ulm.de/nexus/content/repositories/snapshots' ]
end


bash "run_execware_init" do
  cwd "/#{installation_dir}"
  user install_user
  code <<-EOF
   #/usr/lib/jvm/java-8-oracle-amd64/bin/java -jar colosseum-client-test-1.0-SNAPSHOT-jar-with-dependencies.jar -OmistackCloudSecret="#}iVpF14wkdS{^." -GwdgCloudSecret="paasagewp5ostestbed!" -ColosseumUri="http://127.0.0.1:9000/api" -ExampleType="INIT" -WaitingTime=180000
  true
  EOF
end










