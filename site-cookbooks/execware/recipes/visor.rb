



install_user          = node['paasage']['installation_user']
group_id              = 'io.github.cloudiator.visor'

application_name      = "#{node[:execware][:visor][:application_name]}"
version               = "#{node[:execware][:visor][:version]}"
vm_options            = "#{node[:execware][:visor][:vm_options]}"


if node.attribute?('installation_dir')
  installation_dir      = node['execware']['installation_dir']
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:execware][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end


if node.attribute?('pid_file_path')
  pid_file_path      = "#{node[:execware][:pid_file_path]}"
else
  pid_file_path      = "/run/paasage/#{application_name}.pid"
end

directory installation_dir do
  action :create
  mode '0755'
  owner install_user
  group install_user
  recursive true
end

directory config_dir do
  action :create
  mode '0755'
  owner install_user
  group install_user
  recursive true
end

maven application_name do
  group_id    group_id
  version     version
  dest        installation_dir
  owner       install_user
  mode        '0644'
  repositories [ 'https://omi-dev.e-technik.uni-ulm.de/nexus/content/repositories/snapshots' ]
end


template '/etc/paasage/eu.paasage.executionware.metric-collector.properties' do
  source 'eu.paasage.executionware.metric-collector.properties.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({})
end

template '/etc/paasage/visor.properties' do
  source 'visor.properties.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({})
end


template "/etc/init.d/#{application_name}" do
  source 'visor.init.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :install_user =>  "#{install_user}",
                :application_name => "#{application_name}",
                :path => "#{installation_dir}",
                :pid_file_path => "#{pid_file_path}",
                :vm_options => "#{vm_options}",
                :jar => "#{installation_dir}/#{application_name}-#{version}.jar",
                :arguments => '-conf $VISOR_CONFIG_PATH',
                :main_class => 'de.uniulm.omi.cloudiator.visor.Visor',
            })
end


template "/etc/init.d/#{application_name}" do
  source 'execware.backend.init.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :installation_user  => install_user,
                :application_name   => application_name,
                :installation_dir   => installation_dir,
                :jar_file_name      => "#{application_name}-#{version}-jar-with-dependencies.jar"
            })
end

apt_package "jsvc" do
  action :install
end

service "#{application_name}" do
  supports :stop => true, :start => true, :restart => true
  action [ :enable, :restart ]
end



### KAIROSDB

kairosdb_version='1.0.0'
remote_file "/tmp/kairosdb_#{kairosdb_version}-1_all.deb" do
  source "https://github.com/kairosdb/kairosdb/releases/download/v#{kairosdb_version}/kairosdb_#{kairosdb_version}-1_all.deb"
  owner "#{install_user}"
  group "#{install_user}"
  mode "0644"
  action :create
end

dpkg_package 'kairosdb_#{kairosdb_version}-1_all.deb' do
  action :install
  source "/tmp/kairosdb_#{kairosdb_version}-1_all.deb"
end

service "kairosdb" do
  supports :stop => true, :start => true, :restart => true
  action [ :enable, :restart ]
end
