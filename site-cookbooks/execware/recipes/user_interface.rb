#
# Cookbook Name:: webmasterscript
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#



include_recipe 'apache2'
include_recipe 'apache2::mod_php5'

clone_url        = node['execware']['user_interface']['clone_url']
application_name = 'executionware_ui'
install_user     = node['paasage']['installation_user']

if node.attribute?('installation_dir')
  installation_dir      = node['execware']['installation_dir']
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end

apt_package "php5-curl" do
  action :install
end


directory installation_dir do
  action :create
  mode '0755'
  owner install_user
  group install_user
  recursive true
end

# clone repositories
bash "clone-#{application_name}" do
  cwd installation_dir
  user install_user
  group 'www-data'
  code <<-EOH
    rm -rf #{installation_dir}/#{application_name}
    git clone #{clone_url}
  EOH
end



web_app application_name do
  docroot "#{installation_dir}/#{application_name}"
  directory_options ['Indexes', 'FollowSymLinks', 'MultiViews']
  allow_override ['All']
  server_name '10.19.65.101'
end




service "apache2" do
  action :restart
end
