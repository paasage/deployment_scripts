default['execware']['backend']['application_name'] = 'executionware-backend'
default['execware']['backend']['version'] = 'default'
default['execware']['backend']['vm_options']='-Xms512M -Xmx1024M -Xss1M '

default['execware']['visor']['application_name'] = 'visor-service'
default['execware']['visor']['version'] = '1.0.0-SNAPSHOT'
default['execware']['visor']['vm_options']='-Xms512M -Xmx1024M -Xss1M '

default['execware']['execware_init']['application_name'] = 'colosseum-client-test'
default['execware']['execware_init']['version'] = '1.0-SNAPSHOT'
default['execware']['execware_init']['vm_options']='-Xms512M -Xmx1024M -Xss1M '

default['execware']['user_interface']['clone_url'] = 'https://tuleap.ow2.org/plugins/git/paasage/executionware_ui.git'
default['execware']['user_interface']['listen_ip_address'] = '127.0.0.1'

