
default[:metabase][:cerif_mapper][:application_name] = 'cerif_mddb'
default[:metabase][:cerif_mapper][:version] = 'default'
default[:metabase][:cerif_mapper][:vm_options]='-Xms512M -Xmx1024M -Xss1M '

default[:metabase][:cdo_init][:application_name] = 'init'
default[:metabase][:cdo_init][:version] = 'default'
default[:metabase][:cdo_init][:vm_options]='-Xms512M -Xmx1024M -Xss1M '


default[:metabase][:cdo_server][:application_name] = 'server'
default[:metabase][:cdo_server][:version] = 'default'
default[:metabase][:cdo_server][:vm_options]='-Xms512M -Xmx1024M -Xss1M'
default[:metabase][:cdo_server][:dbType]='mysql'
default[:metabase][:cdo_server][:dbHost]='localhost'
default[:metabase][:cdo_server][:dbUser]='cdo_server'
default[:metabase][:cdo_server][:dbPass]='cdo_server_password'
default[:metabase][:cdo_server][:dbName]='cdo_server_database'

