
def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end

classifier            = 'jar-with-dependencies'
group_id              = "#{node[:paasage][:default_group_id]}"
install_user          = "#{node[:paasage][:installation_user]}"
application_name      = "#{node[:metabase][:cdo_init][:application_name]}"
version               = "#{node[:metabase][:cdo_init][:version]}"
vm_options            = "#{node[:metabase][:cdo_init][:vm_options]}"

paasage_version        = node['paasage']['version']
paasage_version_suffix = node['paasage']['version_suffix']
paasage_version_branch = node['paasage']['version_branch']

if version == 'default'
  if paasage_version_branch != ''
    paasage_version_branch = '-' + paasage_version_branch
  end
  if paasage_version_suffix != ''
    paasage_version_suffix = '-' + paasage_version_suffix
  end
  version = "#{paasage_version}#{paasage_version_branch}#{paasage_version_suffix}"
end

if node.attribute?('installation_dir')
  installation_dir      = "#{node[:metabase][:installation_dir]}"
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:metabase][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end

install_user_uid = uid_of_user install_user

if node.attribute?('pid_file_path')
  pid_file_path      = "#{node[:metabase][:pid_file_path]}"
else
  pid_file_path      = "/run/paasage/#{application_name}.pid"
end


directory "#{installation_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end

directory "#{config_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end


maven "#{application_name}" do
  group_id    "#{group_id}"
  version     "#{version}"
  classifier  "#{classifier}"
  dest        "#{installation_dir}"
  owner       "#{install_user}"
  mode        '0644'
end