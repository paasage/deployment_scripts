#
# Cookbook Name:: metabase
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#include_recipe 'metabase::cerif_mapper'
include_recipe 'metabase::cdo_server'
include_recipe 'metabase::cdo_init'

