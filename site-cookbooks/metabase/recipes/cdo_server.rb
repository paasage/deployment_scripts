include_recipe 'mysql::server'
include_recipe 'database::mysql'

def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end

group_id              = "#{node[:paasage][:default_group_id]}"
install_user          = "#{node[:paasage][:installation_user]}"
application_name      = "#{node[:metabase][:cdo_server][:application_name]}"
version               = "#{node[:metabase][:cdo_server][:version]}"
classifier            = 'jar-with-dependencies'
vm_options            = "#{node[:metabase][:cdo_server][:vm_options]}"
db_type               = "#{node[:metabase][:cdo_server][:dbType]}"
db_host               = "#{node[:metabase][:cdo_server][:dbHost]}"
db_user               = "#{node[:metabase][:cdo_server][:dbUser]}"
db_password           = "#{node[:metabase][:cdo_server][:dbPass]}"
db_name               = "#{node[:metabase][:cdo_server][:dbName]}"
db_url                = "jdbc:#{db_type}://#{db_host}:3306/#{db_name}"
paasage_version        = node['paasage']['version']
paasage_version_suffix = node['paasage']['version_suffix']
paasage_version_branch = node['paasage']['version_branch']

if version == 'default'
  if paasage_version_branch != ''
    paasage_version_branch = '-' + paasage_version_branch
  end
  if paasage_version_suffix != ''
    paasage_version_suffix = '-' + paasage_version_suffix
  end
  version = "#{paasage_version}#{paasage_version_branch}#{paasage_version_suffix}"
end

if node.attribute?('installation_dir')
  installation_dir      = "#{node[:metabase][:cdo_server][:installation_dir]}"
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:metabase][:cdo_server][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end

install_user_uid = uid_of_user install_user

if node.attribute?('pid_file_path')
  pid_file_path      = "#{node[:metabase][:cdo_server][:pid_file_path]}"
else
  pid_file_path      = "/run/paasage/#{application_name}.pid"
end


directory "#{installation_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end

directory "#{config_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end


maven "#{application_name}" do
  group_id    "#{group_id}.mddb.cdo"
  version     "#{version}"
  classifier  "#{classifier}"
  dest        "#{installation_dir}"
  owner       "#{install_user}"
  mode        '0644'
end
# create a database
mysql_connection_info = {
    :host     => 'localhost',
    :username => 'root',
    :password => node['mysql']['server_root_password']
}

mysql_database "#{node[:metabase][:cdo_server][:dbName]}" do
  connection mysql_connection_info
  action :create
end

mysql_database_user "#{node[:metabase][:cdo_server][:dbUser]}" do
  connection mysql_connection_info
  password "#{node[:metabase][:cdo_server][:dbPass]}"
  action :create
end

mysql_database_user "#{node[:metabase][:cdo_server][:dbUser]}" do
  connection    mysql_connection_info
  password      "#{node[:metabase][:cdo_server][:dbPass]}"
  database_name "#{node[:metabase][:cdo_server][:dbName]}"
  action        :grant
end

template "/etc/init.d/#{application_name}" do
  source 'initd.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :install_user =>  "#{install_user}",
                :application_name => "#{application_name}",
                :path => "#{installation_dir}",
                :pid_file_path => "#{pid_file_path}",
                :vm_options => "#{vm_options}",
                :jar => "#{installation_dir}/#{application_name}-#{version}-#{classifier}.jar",
                :arguments => '',
                :main_class => 'eu.paasage.mddb.cdo.server.MyCDOServer',
            })
end

template '/etc/paasage/eu.paasage.mddb.cdo.server.properties' do
  source 'cdo_server/server.properties.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :db_type     => db_type,
                :db_url      => db_url,
                :db_user     => db_user,
                :db_password => db_password,
            })
end

template '/etc/paasage/eu.paasage.mddb.mysqldump.server.properties' do
  source 'mysqldump/mysqldumpserver.properties.erb'
  owner 'root'
  group 'root'
  mode '0744'
  variables({
                :db_type     => db_type,
                :db_url      => db_url,
                :db_user     => db_user,
                :db_password => db_password,
                :db_name => db_name,
                :db_host => db_host,
            })
end

service "#{application_name}" do
  supports :stop => true, :start => true, :restart => true
  action [ :enable, :restart ]
end
