#
# Cookbook Name:: netifs
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


ruby_block "set-nameserver" do
  block do
    File.write("/etc/resolv.conf", "nameserver #{node[:netifs][:nameserver]}\n")
  end
end

route "0.0.0.0/0" do
  gateway node[:netifs][:gateway]
  device node[:netifs][:interface]
end


bash "ping-gateway" do
  user "root"
  group "root"
  code <<-EOH
    ping -c 4 #{node[:netifs][:gateway]}
  EOH
end
