

default[:profiler][:rule_processor][:application_name] = 'rule-processor'
default[:profiler][:rule_processor][:version]          = 'default'
default[:profiler][:rule_processor][:vm_options]       = '-Xms512M -Xmx1024M -Xss1M '


default[:profiler][:generator][:application_name]      = 'generator'
default[:profiler][:generator][:version]               = 'default'
default[:profiler][:generator][:vm_options]            ='-Xms512M -Xmx1024M -Xss1M '

