


install_user          = "#{node[:paasage][:installation_user]}"
group_id              = "#{node[:paasage][:default_group_id]}"

application_name      = "#{node[:profiler][:rule_processor][:application_name]}"
version               = "#{node[:profiler][:rule_processor][:version]}"
vm_options            = "#{node[:profiler][:rule_processor][:vm_options]}"

paasage_version        = node['paasage']['version']
paasage_version_suffix = node['paasage']['version_suffix']
paasage_version_branch = node['paasage']['version_branch']

if version == 'default'
  if paasage_version_branch != ''
    paasage_version_branch = '-' + paasage_version_branch
  end
  if paasage_version_suffix != ''
    paasage_version_suffix = '-' + paasage_version_suffix
  end
  version = "#{paasage_version}#{paasage_version_branch}#{paasage_version_suffix}"
end

def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end

if node.attribute?('installation_dir')
  installation_dir      = "#{node[:profiler][:installation_dir]}"
else
  installation_dir      = "/home/#{install_user}/#{application_name}/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:profiler][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end

install_user_uid = uid_of_user install_user

if node.attribute?('pid_file_path')
  pid_file_path      = "#{node[:profiler][:pid_file_path]}"
else
  pid_file_path      = "/run/paasage/#{application_name}.pid"
end

directory "#{installation_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end

directory "#{config_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end

maven "#{application_name}" do
  group_id    "#{group_id}"
  version     "#{version}"
  classifier  'jar-with-dependencies'
  dest        "#{installation_dir}"
  owner       "#{install_user}"
  mode        '0644'
end







template "/etc/init.d/#{application_name}" do
  source "initd.erb"
  owner "root"
  group "root"
  mode "0744"
  variables({
                :install_user =>  "#{install_user}",
                :application_name => "#{application_name}",
                :path => "#{installation_dir}",
                :pid_file_path => "#{pid_file_path}",
                :vm_options => "#{vm_options}",
                :jar => "#{installation_dir}/#{application_name}.jar",
                :arguments => "",
                :main_class => "eu.paasage.upperware.profiler.rp/RuleProcessor",
            })
end

template "/etc/paasage/wp3_profiler.properties" do
  source "rule_processor/wp3_profiler.properties.erb"
  owner "root"
  group "root"
  mode "0744"
  variables({
                :run_as =>  "#{install_user}",
                :application_name => "#{application_name}",
            })
end

service "#{application_name}" do
  supports :stop => true, :start => true, :restart => true
  action [ :enable, :restart ]
end

#