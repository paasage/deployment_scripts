default['play_app']['application_name'] = 'colosseum'
default['play_app']['dist_url'] = "https://omi-dev.e-technik.uni-ulm.de/jenkins/job/cloudiator-colosseum/lastSuccessfulBuild/artifact/target/universal"
default['play_app']['dist_name'] = "colosseum-0.2.0-SNAPSHOT"
default['play_app']['pid_file_name']="colosseum.pid"
default['play_app']['application_secret_key']="Your Secret Key here"
default['play_app']['external_ip_address']="127.0.0.1"

default['play_app']['dbUser']="play"
default['play_app']['dbPass']="playSecretDatabasePassword"
default['play_app']['dbName']="play"
default['play_app']['dbHost']="localhost"

default['play_app']['play_log_level']="INFO"  
default['play_app']['app_log_level']="DEBUG"
default['play_app']['language']="en"