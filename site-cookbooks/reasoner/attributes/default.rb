


default[:reasoner][:milp_solver][:application_name] = 'milp-solver'
default[:reasoner][:milp_solver][:version] = 'default'
default[:reasoner][:milp_solver][:vm_options]='-Xms512M -Xmx1024M -Xss1M -XX:MaxPermSize=512M'

# local_url
default[:reasoner][:milp_solver][:cmpl_url]='http://jenkins.paasage.cetic.be/repository/Cmpl-1-10-0-linux64.tar.gz'

default[:reasoner][:la_converter][:application_name] = 'la-converter'
default[:reasoner][:la_converter][:version] = 'default'
default[:reasoner][:la_converter][:vm_options]='-Xms512M -Xmx1024M -Xss1M -XX:MaxPermSize=512M'


default[:reasoner][:solver_to_deployment][:application_name] = 'solver-to-deployment'
default[:reasoner][:solver_to_deployment][:version] = 'default'
default[:reasoner][:solver_to_deployment][:vm_options]='-Xms512M -Xmx1024M -Xss1M -XX:MaxPermSize=512M'

default[:reasoner][:meta_solver][:application_name] = 'meta_solver'
default[:reasoner][:meta_solver][:version]='default'
default[:reasoner][:meta_solver][:vm_options]='-Xms512M -Xmx1024M -Xss1M -XX:MaxPermSize=512M'

default[:reasoner][:la_based_reasoner][:application_name] = 'la_based_reasoner'
default[:reasoner][:la_based_reasoner][:clone_url] = 'http://paasage_demo:PaaSageReview@git.cetic.be/paasage'


default[:reasoner][:cp_solver][:application_name] = 'cp-solver'
default[:reasoner][:cp_solver][:version] = 'default'
default[:reasoner][:cp_solver][:vm_options]='-Xms512M -Xmx1024M -Xss1M -XX:MaxPermSize=512M'
