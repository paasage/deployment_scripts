


install_user          = "#{node[:paasage][:installation_user]}"
application_name      = "#{node[:reasoner][:la_based_reasoner][:application_name]}"
clone_url             = "#{node[:reasoner][:la_based_reasoner][:clone_url]}"

def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end




if node.attribute?('installation_dir')
  installation_dir      = "#{node[:reasoner][:installation_dir]}"
else
  installation_dir      = "/home/#{install_user}/#{application_name}"
end



directory "#{installation_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end

apt_package "git" do
  action :install
end



apt_package "g++" do
  action :install
end

apt_package "libnlopt-dev" do
  action :install
end


apt_package "libboost-dev" do
  action :install
end


# clone repositories
bash "clone-#{application_name}" do
  cwd "#{installation_dir}"
  user install_user
  group install_user
  code <<-EOH
    rm -rf #{installation_dir}/#{application_name}
    mkdir -p #{installation_dir}/#{application_name}
    cd #{installation_dir}/#{application_name}
    git clone #{clone_url}/la-based-resoner.git
    git clone #{clone_url}/theron.git
    git clone #{clone_url}/la-framework.git
  EOH
end


bash "build theron-#{application_name}" do
  cwd "/#{installation_dir}/#{application_name}/theron"
  user install_user
  group install_user
  code <<-EOH
    make library
  EOH
end

bash "build #{application_name}" do
  cwd "/#{installation_dir}/#{application_name}/la-based-resoner"
  user install_user
  group install_user
  code <<-EOH
    export NLOPT_LIB=-lnlopt
    export THERON=../theron
    export LA_FRAMEWORK=../la-framework/
    make LASolver
  EOH
end
