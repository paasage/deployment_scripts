


group_id              = "#{node[:paasage][:default_group_id]}"
install_user          = "#{node[:paasage][:installation_user]}"
application_name      = "#{node[:reasoner][:milp_solver][:application_name]}"
version               = "#{node[:reasoner][:milp_solver][:version]}"
cmpl_url              = "#{node[:reasoner][:milp_solver][:cmpl_url]}"
vm_options            = "#{node[:reasoner][:milp_solver][:vm_options]}"
paasage_version        = node['paasage']['version']
paasage_version_suffix = node['paasage']['version_suffix']
paasage_version_branch = node['paasage']['version_branch']

if version == 'default'
  if paasage_version_branch != ''
    paasage_version_branch = '-' + paasage_version_branch
  end
  if paasage_version_suffix != ''
    paasage_version_suffix = '-' + paasage_version_suffix
  end
  version = "#{paasage_version}#{paasage_version_branch}#{paasage_version_suffix}"
end

def uid_of_user(username)
  node['etc']['passwd'].each do |user, data|
    if user = username
      return data['uid']
    end
    return 0
  end
end




if node.attribute?('installation_dir')
  installation_dir      = "#{node[:reasoner][:installation_dir]}"
else
  installation_dir      = "/home/#{install_user}/solvers/app"
end


if node.attribute?('config_dir')
  config_dir      = "#{node[:reasoner][:config_dir]}"
else
  config_dir      = "/home/#{install_user}/#{application_name}/config"
end

install_user_uid = uid_of_user install_user

if node.attribute?('pid_file_path')
  pid_file_path      = "#{node[:reasoner][:pid_file_path]}"
else
  pid_file_path      = "/run/paasage/#{application_name}.pid"
end


directory "#{installation_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end

directory "#{config_dir}" do
  action :create
  mode "0755"
  owner "#{install_user}"
  group "#{install_user}"
  recursive true
end


# install dependencies
package "libqtcore4" do
  action :install
end

package "libqt4-xml" do
  action :install
end

# Download cmpl
remote_file "/tmp/Cmpl.tar.gz" do
  source "http://jenkins.paasage.cetic.be/repository/Cmpl-1-10-0-linux64.tar.gz"
  owner "#{install_user}"
  group "#{install_user}"
  mode "0644"
  action :create_if_missing
end


# Install Cmpl
bash "install_cmpl" do
  #user install_user
  cwd "/tmp"
  code <<-EOH
    sudo rm -fr ./Cmpl
    tar xvfz Cmpl.tar.gz
    cd Cmpl
    sudo ./install
  EOH
end

maven "#{application_name}" do
  group_id    "#{group_id}"
  version     "#{version}"
  classifier  'assembly'
  dest        "#{installation_dir}"
  owner       "#{install_user}"
  mode        '0644'
end

link "#{installation_dir}/milp-solver-assembly.jar" do
  to "#{installation_dir}/#{application_name}-#{version}-assembly.jar"
  link_type :hard
end