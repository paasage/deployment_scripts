#
# Cookbook Name:: reasoner
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'reasoner::milp_solver'
include_recipe 'reasoner::la_converter'
include_recipe 'reasoner::solver_to_deployment'
include_recipe 'reasoner::meta_solver'
include_recipe 'reasoner::la_based_reasoner'
include_recipe 'reasoner::cp_solver'

